const mongoose = require("mongoose");
const Product = require ("../models/product.js");

module.exports.addProduct = (reqBody, newData) => {
	if(newData.isAdmin == true){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
			slots: reqBody.slots
		})
		return newProduct.save().then((newProduct, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	

}