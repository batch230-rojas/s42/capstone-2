const express = require("express");
const router = express.Router();
const productControllers =  require("../controllers/productControllers.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	productControllers.addCourse(request.body, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


router.get("/active", (request, response) => {
	productControllers.getActiveProduct().then(resultFromController => response.send(resultFromController))
});


module.exports = router;