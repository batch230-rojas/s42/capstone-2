const mongoose = require("mongoose");

		const userSchema = new mongoose.Schema({
			firstName : {
				type : String,
				required : [true, "First name is required"]
			},
			lastName : {
				type : String,
				required : [true, "Last name is required"]
			},
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			mobileNo : {
				type : String, 
				required : [true, "Mobile No is required"]
			},
			isAdmin : {
				type : Boolean,
				default : true
			},
			orders : [
				{
					totalAmount : {
						type : Number,
						required : [true, "Total is required"]
					},
					purchasedOn : {
						type : Date,
						default : new Date()
					},
					products :[
						{
							productId : {
								type :String,
								required : [true, "Product ID is required"]
							},
							productName : {
								type :String,
								required : [true, "Product name is required"]
							},
							quantity : {
								type: Number,
								required: [true, "Quantity is required"]
							}
						}
					]
				}
			]
		}
	)


module.exports = mongoose.model("user", userSchema);